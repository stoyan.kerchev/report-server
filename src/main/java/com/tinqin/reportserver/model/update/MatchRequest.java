package com.tinqin.reportserver.model.update;

import lombok.*;

import java.util.List;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MatchRequest {
        private List<Integer> idList;
        private List<String> statuses;
        private List<String> priorities;
}
