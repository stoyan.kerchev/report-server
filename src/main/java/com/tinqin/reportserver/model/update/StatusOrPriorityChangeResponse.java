package com.tinqin.reportserver.model.update;

import lombok.*;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class StatusOrPriorityChangeResponse {
    private Integer idList;
    private String from;
    private String to;
}
