package com.tinqin.reportserver.model.update;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UpdateStatusOrPriorityResponse {
    private LocalDateTime dataTime;
    private List<StatusOrPriorityChangeResponse> changes;
}
