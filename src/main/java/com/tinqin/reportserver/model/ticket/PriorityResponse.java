package com.tinqin.reportserver.model.ticket;

import lombok.*;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PriorityResponse {
    private Integer id;
    private String name;
}
