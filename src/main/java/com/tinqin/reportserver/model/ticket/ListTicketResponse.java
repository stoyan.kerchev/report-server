package com.tinqin.reportserver.model.ticket;

import lombok.*;

import java.util.Map;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ListTicketResponse {
    private Map<String, Integer> statuses;
    private Map<String, Integer> assignees;
    private Map<String, Integer> priorities;
    private int ticketCount;
}
