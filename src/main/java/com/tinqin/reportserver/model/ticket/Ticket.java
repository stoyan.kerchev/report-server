package com.tinqin.reportserver.model.ticket;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Ticket {
    private Integer id;
    private String title;
    private String description;
    private Integer createdBy;
    private String priority;
    private LocalDateTime createdAt;
    private String status;
    private LocalDateTime statusChangedAt;
    private Boolean archived;
}
