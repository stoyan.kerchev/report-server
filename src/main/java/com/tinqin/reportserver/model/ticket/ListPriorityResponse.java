package com.tinqin.reportserver.model.ticket;

import lombok.*;

import java.util.List;

@Builder
@Getter
@ToString
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ListPriorityResponse {
    List<PriorityResponse> priorityResponseList;
}
