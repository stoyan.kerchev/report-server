package com.tinqin.reportserver.service;

import com.tinqin.reportserver.datasource.contract.TicketManagerSource;
import com.tinqin.reportserver.datasource.exceptions.ExternalSystemException;
import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;
import com.tinqin.reportserver.model.ticket.TicketManagerResponse;
import com.tinqin.reportserver.service.contract.TicketManagerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketManagerServiceImpl implements TicketManagerService {
    public static final int REJECTED = 4;
    private final TicketManagerSource ticketManagerSource;

    public TicketManagerServiceImpl(final TicketManagerSource ticketManagerSource) {
        this.ticketManagerSource = ticketManagerSource;
    }

    @Override
    public TicketManagerResponse closeTicket(final int id) {
        try {
            final Ticket ticketResponse = ticketManagerSource.readTicketById(id);
            ticketManagerSource.changeStatus(id, REJECTED);
            return TicketManagerResponse.builder()
                    .ticketId(id)
                    .initialStatus(ticketResponse.getStatus())
                    .errorMessage("6te go proverim v datasource dali e OK ili ne?")
                    .result("success,no pak drugade se proveriava").build();
        } catch (final ExternalSystemException e) {
            throw new ExternalSystemException(e.getMessage());
        }
    }

    @Override
    public Ticket archiveTicket(final int id) {
        try {
            final Ticket ticket = ticketManagerSource.readTicketById(id);
            if (!ticket.getArchived()) {
                final Ticket archivedTicket = Ticket.builder()
                        .id(ticket.getId())
                        .title(ticket.getTitle())
                        .description(ticket.getDescription())
                        .createdBy(ticket.getCreatedBy())
                        .priority(ticket.getPriority())
                        .createdAt(ticket.getCreatedAt())
                        .status(ticket.getStatus())
                        .statusChangedAt(ticket.getStatusChangedAt())
                        .archived(true)
                        .build();
                ticketManagerSource.archiveTicket(archivedTicket);
                return archivedTicket;
            }
            return ticket;
        } catch (final ExternalSystemException e) {
            throw new ExternalSystemException(e.getMessage());
        }
    }

    @Override
    public List<StatusResponse> getTicketStatuses() {
        return ticketManagerSource.getTicketStatuses();
    }
}
