package com.tinqin.reportserver.service;

import com.tinqin.reportserver.datasource.contract.TicketManagerSource;
import com.tinqin.reportserver.model.ticket.PriorityResponse;
import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;
import com.tinqin.reportserver.model.update.StatusOrPriorityChangeResponse;
import com.tinqin.reportserver.model.update.UpdatePriorityRequest;
import com.tinqin.reportserver.model.update.UpdateStatusOrPriorityResponse;
import com.tinqin.reportserver.model.update.UpdateStatusRequest;
import com.tinqin.reportserver.service.contract.BulkUpdatesService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BulkUpdatesServiceImpl implements BulkUpdatesService {
    private final TicketManagerSource ticketManagerSource;

    public BulkUpdatesServiceImpl(final TicketManagerSource ticketManagerSource) {
        this.ticketManagerSource = ticketManagerSource;
    }

    @Override
    public UpdateStatusOrPriorityResponse updateStatus(final UpdateStatusRequest request) {
        final List<Ticket> tickets = ticketManagerSource.getAll();
        final List<StatusResponse> statusResponseList = ticketManagerSource.getTicketStatuses();
        final Map<String,Integer> statuses = new HashMap<>();
        final List<StatusOrPriorityChangeResponse> changes = new ArrayList<>();
        for (final StatusResponse statusResponse : statusResponseList) {
            statuses.put(statusResponse.getName(),statusResponse.getId());
        }
        for (final Ticket ticket : tickets) {
            if (request.getMatch().getStatuses().contains(ticket.getStatus())
            && request.getMatch().getPriorities().contains(ticket.getPriority())) {
                changes.add(StatusOrPriorityChangeResponse.builder()
                        .idList(ticket.getId()).from(ticket.getStatus()).to(request.getMoveTo()).build());
                ticketManagerSource.changeStatus(ticket.getId(),statuses.get(request.getMoveTo()));
            }
        }
        return UpdateStatusOrPriorityResponse.builder().dataTime(LocalDateTime.now()).changes(changes).build();
    }

    //todo implement increase decrease logic
    @Override
    public UpdateStatusOrPriorityResponse updatePriority(final UpdatePriorityRequest request) {
        final List<Ticket> tickets = ticketManagerSource.getAll();
        final List<PriorityResponse> priorityResponseList = ticketManagerSource.getTicketPriorities();
        final Map<String,Integer> priorities = new HashMap<>();
        final List<StatusOrPriorityChangeResponse> changes = new ArrayList<>();
        for (final PriorityResponse priorityResponse : priorityResponseList) {
            priorities.put(priorityResponse.getName(),priorityResponse.getId());
        }
        for (final Ticket ticket : tickets) {
            if (request.getMatch().getStatuses().contains(ticket.getStatus())
                    && request.getMatch().getPriorities().contains(ticket.getPriority())) {
                changes.add(StatusOrPriorityChangeResponse.builder()
                        .idList(ticket.getId()).from(ticket.getPriority()).to(request.getMoveTo()).build());
                ticketManagerSource.changePriority(ticket.getId(),priorities.get(request.getMoveTo()));
            }
        }
        return UpdateStatusOrPriorityResponse.builder().dataTime(LocalDateTime.now()).changes(changes).build();
    }

}
