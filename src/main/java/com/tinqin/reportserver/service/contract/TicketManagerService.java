package com.tinqin.reportserver.service.contract;

import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;
import com.tinqin.reportserver.model.ticket.TicketManagerResponse;

import java.util.List;

public interface TicketManagerService {
    TicketManagerResponse closeTicket(int id);

    Ticket archiveTicket(int id);

    List<StatusResponse> getTicketStatuses();
}
