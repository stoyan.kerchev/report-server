package com.tinqin.reportserver.service.contract;

import com.tinqin.reportserver.model.update.UpdatePriorityRequest;
import com.tinqin.reportserver.model.update.UpdateStatusOrPriorityResponse;
import com.tinqin.reportserver.model.update.UpdateStatusRequest;

public interface BulkUpdatesService {
    UpdateStatusOrPriorityResponse updateStatus(UpdateStatusRequest request);

    UpdateStatusOrPriorityResponse updatePriority(UpdatePriorityRequest request);
}
