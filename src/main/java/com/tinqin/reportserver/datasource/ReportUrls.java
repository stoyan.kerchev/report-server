package com.tinqin.reportserver.datasource;

public class ReportUrls {

    public static final String API_ROOT = "/api";

    public static final String API_TICKETS = "/tickets";

    public static final String API_ROOT_TICKETS = "/api/tickets";

    public static final String CHANGE_TICKET_STATUS = "/api/tickets/status";

    public static final String CHANGE_TICKET_PRIORITY = "/api/tickets/priority";

    public static final String GET_TICKET_STATUSES = "/api/tickets/statuses";

    public static final String GET_TICKET_PRIORITIES = "/api/tickets/priorities";
}
