package com.tinqin.reportserver.datasource;

import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public class WebResourceClient {

    private final RestTemplate restTemplate = new RestTemplate();
    private final HttpHeaders headers = new HttpHeaders();

    public WebResourceClient() {
    }

    public String getUrl() {
        return "http://localhost:8080";
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }
}
