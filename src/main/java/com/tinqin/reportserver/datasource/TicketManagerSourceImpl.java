package com.tinqin.reportserver.datasource;

import com.tinqin.reportserver.datasource.contract.TicketManagerSource;
import com.tinqin.reportserver.datasource.exceptions.ExternalSystemException;
import com.tinqin.reportserver.model.ticket.PriorityResponse;
import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static com.tinqin.reportserver.datasource.ReportUrls.*;

@Component
public class TicketManagerSourceImpl implements TicketManagerSource {
    private final WebResourceClient webResourceClient = new WebResourceClient();

    @Override
    public Ticket readTicketById(final int id) {
        try {
            final URI uri = new URI(webResourceClient.getUrl() + API_ROOT_TICKETS + "/" + id);
            return webResourceClient.getRestTemplate().getForObject(
                    uri, Ticket.class);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation get ticket return" + e.getMessage());
        }
    }

    @Override
    public void changeStatus(final int ticketId, final int statusId) {
        try {
            final HttpEntity<String> requestUpdate = new HttpEntity<>("change status");
            webResourceClient.getRestTemplate().exchange(
                    webResourceClient.getUrl() + CHANGE_TICKET_STATUS + "/" + ticketId + "/" + statusId, HttpMethod.PUT,
                    requestUpdate, Ticket.class);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation change ticket status return " + e.getMessage());
        }
    }

    @Override
    public void changePriority(final int ticketId, final int priorityId) {
        try {
            final HttpEntity<String> requestUpdate = new HttpEntity<>("change priority");
            webResourceClient.getRestTemplate().exchange(
                    webResourceClient.getUrl() + CHANGE_TICKET_PRIORITY + "/" + ticketId + "/" + priorityId, HttpMethod.PUT,
                    requestUpdate, Ticket.class);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation change ticket priority return " + e.getMessage());
        }
    }

    @Override
    public void archiveTicket(final Ticket ticket) {
        try {
            final HttpEntity<Ticket> requestUpdate = new HttpEntity<>(ticket);
            webResourceClient.getRestTemplate().exchange(
                    webResourceClient.getUrl() + API_ROOT_TICKETS + "/" + ticket.getId(), HttpMethod.PUT,
                    requestUpdate, Ticket.class);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation archive ticket return " + e.getMessage());
        }
    }


    @Override
    public List<Ticket> getAll() {
        try {
            final URI uri = new URI(webResourceClient.getUrl() + API_ROOT_TICKETS);
            final Ticket[] tickets = webResourceClient.getRestTemplate().getForObject(
                    uri, Ticket[].class);
            assert tickets != null;
            return Arrays.asList(tickets);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation get ticket return" + e.getMessage());
        }
    }

    //todo ticketstatuses and priorities in one pojo and one method
    @Override
    public List<StatusResponse> getTicketStatuses() {
        try {
            final URI uri = new URI(webResourceClient.getUrl() + GET_TICKET_STATUSES);
            final StatusResponse[] statusResponse = webResourceClient.getRestTemplate().getForObject(
                    uri, StatusResponse[].class);
            assert statusResponse != null;
            return Arrays.asList(statusResponse);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation get ticket return" + e.getMessage());
        }
    }

    @Override
    public List<PriorityResponse> getTicketPriorities() {
        try {
            final URI uri = new URI(webResourceClient.getUrl() + GET_TICKET_PRIORITIES);
            final PriorityResponse[] priorityResponse = webResourceClient.getRestTemplate().getForObject(
                    uri, PriorityResponse[].class);
            assert priorityResponse != null;
            return Arrays.asList(priorityResponse);
        } catch (final Exception e) {
            throw new ExternalSystemException("Operation get ticket return" + e.getMessage());
        }
    }
}
