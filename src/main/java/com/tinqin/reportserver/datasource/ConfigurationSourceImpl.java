package com.tinqin.reportserver.datasource;

import com.tinqin.reportserver.datasource.contract.ConfigurationSource;
import com.tinqin.reportserver.model.ApplicationInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConfigurationSourceImpl implements ConfigurationSource {

    private final String applicationVersion;
    private final String applicationName;

    public ConfigurationSourceImpl(
            @Value("${application.version}") String applicationVersion,
            @Value("${application.name}") String applicationName) {
        this.applicationVersion = applicationVersion;
        this.applicationName = applicationName;
    }

    @Override
    public Optional<String> applicationVersion() {
        return Optional.ofNullable(applicationVersion);
    }

    @Override
    public ApplicationInfo applicationInfo() {
        return ApplicationInfo.builder().name(applicationName).version(applicationVersion).build();
    }


}
