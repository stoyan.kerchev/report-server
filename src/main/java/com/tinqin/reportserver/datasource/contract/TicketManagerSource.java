package com.tinqin.reportserver.datasource.contract;

import com.tinqin.reportserver.model.ticket.PriorityResponse;
import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;

import java.util.List;

public interface TicketManagerSource {

    Ticket readTicketById(int id);

    void changeStatus(int ticketId, int statusId);

    void changePriority(int ticketId, int priorityId);

    void archiveTicket(Ticket ticket);

    List<StatusResponse> getTicketStatuses();

    List<Ticket> getAll();

    List<PriorityResponse> getTicketPriorities();
}
