package com.tinqin.reportserver.datasource.contract;

import com.tinqin.reportserver.model.ticket.ListTicketResponse;
import com.tinqin.reportserver.model.ticket.Ticket;

public interface AdminService {
    Ticket ticketCount(ListTicketResponse listTicketResponse);
}
