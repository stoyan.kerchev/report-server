package com.tinqin.reportserver.datasource.contract;

import com.tinqin.reportserver.model.ApplicationInfo;

import java.util.Optional;

public interface ConfigurationSource {

    Optional<String> applicationVersion();

    ApplicationInfo applicationInfo();
}
