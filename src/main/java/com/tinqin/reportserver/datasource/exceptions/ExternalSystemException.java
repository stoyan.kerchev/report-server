package com.tinqin.reportserver.datasource.exceptions;

public class ExternalSystemException extends RuntimeException{
    public ExternalSystemException(final String message) {
        super(message);
    }
}
