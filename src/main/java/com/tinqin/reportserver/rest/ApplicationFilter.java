package com.tinqin.reportserver.rest;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebFilter("/*")
@Component
public class ApplicationFilter implements Filter {
    private LocalDateTime time;

    public ApplicationFilter() {
    }

    public ApplicationFilter(final LocalDateTime time) {
        this.time = time;
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        final String noCode = "No code here";
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
                         final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        httpServletResponse.setHeader("time", String.valueOf(time));
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        final String noCode = "No code here";
    }
}