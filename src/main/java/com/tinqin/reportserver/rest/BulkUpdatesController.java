package com.tinqin.reportserver.rest;

import com.tinqin.reportserver.model.update.UpdatePriorityRequest;
import com.tinqin.reportserver.model.update.UpdateStatusOrPriorityResponse;
import com.tinqin.reportserver.model.update.UpdateStatusRequest;
import com.tinqin.reportserver.service.contract.BulkUpdatesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BulkUpdatesController {
    private final BulkUpdatesService bulkUpdatesService;

    @PostMapping(value = "/api/v1/tickets/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpdateStatusOrPriorityResponse> updateStatus(@RequestBody final UpdateStatusRequest request) {
        return ResponseEntity.ok(bulkUpdatesService.updateStatus(request));
    }

    @PostMapping(value = "/api/v1/tickets/priority", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpdateStatusOrPriorityResponse> updatePriority(@RequestBody final UpdatePriorityRequest request) {
        return ResponseEntity.ok(bulkUpdatesService.updatePriority(request));
    }
}
