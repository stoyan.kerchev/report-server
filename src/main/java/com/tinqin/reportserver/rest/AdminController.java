package com.tinqin.reportserver.rest;

import com.tinqin.reportserver.datasource.contract.ConfigurationSource;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AdminController {

    private final ConfigurationSource configurationSource;

    @GetMapping(value = "/api/v1/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getInfo() {
        return ResponseEntity.ok(configurationSource.applicationInfo());
    }

    @GetMapping(value = "/api/v1/ping-not-secure", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> ping() {
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/v1/ping", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> pingWithAuthentication() {
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/v1/version", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVersion() {
        return ResponseEntity.ok(configurationSource.applicationVersion().orElse("unknown"));
    }

//    @GetMapping(value = "/api/v1/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<String> listTickets() {
//        final RestTemplate restTemplate = new RestTemplate();
//        final String fooResourceUrl
//                = "http://localhost:8080/api/tickets/list-tickets";
//        return restTemplate.getForEntity(fooResourceUrl, String.class);
//    }
//
//    @GetMapping(value = "/api/v1/tickets2", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<ListTicketResponse> listTicketsAsObject() {
//        final RestTemplate restTemplate = new RestTemplate();
//        final String fooResourceUrl
//                = "http://localhost:8080/api/tickets/list-tickets";
//        final ListTicketResponse forObject = restTemplate.getForObject(fooResourceUrl, ListTicketResponse.class);
//        return ResponseEntity.ok(forObject);
//    }
//
//    @PostMapping(value = "/api/v1/tickets/report", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Ticket> create() {
//        final RestTemplate restTemplate = new RestTemplate();
//        final String fooResourceUrl
//                = "http://localhost:8080/api/tickets/list-tickets";
//        final ListTicketResponse listTicketResponse = restTemplate.getForObject(fooResourceUrl, ListTicketResponse.class);
//        final Ticket ticketReport = adminService.ticketCount(listTicketResponse);
//        final Ticket ticket = restTemplate.postForObject("http://localhost:8080/api/tickets", ticketReport, Ticket.class);
//        return ResponseEntity.ok(ticket);
//    }
}
