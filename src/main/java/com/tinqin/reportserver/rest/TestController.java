package com.tinqin.reportserver.rest;

import com.tinqin.reportserver.datasource.contract.TicketManagerSource;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final TicketManagerSource ticketManagerSource;

    @GetMapping(value = "/getTicket", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> test() {
        return ResponseEntity.ok(ticketManagerSource.readTicketById(1));
    }
}
