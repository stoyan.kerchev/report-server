package com.tinqin.reportserver.rest;

import com.tinqin.reportserver.model.ticket.StatusResponse;
import com.tinqin.reportserver.model.ticket.Ticket;
import com.tinqin.reportserver.model.ticket.TicketManagerResponse;
import com.tinqin.reportserver.service.contract.TicketManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class TicketManagerController {
    private final TicketManagerService ticketManagerService;

    @GetMapping(value = "/api/v1/tickets/close", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketManagerResponse> closeTicket(@RequestParam final int id) {
        return ResponseEntity.ok(ticketManagerService.closeTicket(id));
    }

    @PutMapping(value = "/api/v1/tickets/archive", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Ticket> archiveTicket(@RequestParam final int id) {
        return ResponseEntity.ok(ticketManagerService.archiveTicket(id));
    }

    @GetMapping(value = "/api/v1/tickets/statuses", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<StatusResponse>> getTicketStatuses() {
        return ResponseEntity.ok(ticketManagerService.getTicketStatuses());
    }

//    @GetMapping(value = "/api/v1/tickets/priorities", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<TicketManagerResponse> getTicketPriorities() {
//        return ResponseEntity.ok(ticketManagerService.getTicketPriorities());
//    }
}
