package com.tinqin.reportserver.rest;

import com.tinqin.reportserver.datasource.exceptions.ExternalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerAdvice {

    private final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

    @ExceptionHandler(ExternalSystemException.class)
    public ResponseEntity<Object> handleUnauthorizedOperationException(final ExternalSystemException exception) {
        LOGGER.debug("Constraint Violation Exception: {}", exception.getMessage());
        LOGGER.trace("Constraint Violation Exception:", exception);
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ExternalSystemException(exception.getMessage()));
    }
}
